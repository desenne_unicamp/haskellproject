module ProjectHaskell where

-- author: Carolina de Senne Garcia
-- desennecarol@gmail.com

-- load this file in ghci
-- ghci project1.hs

-- run this program
-- runhaskell project1.hs

-- run this program with an input from file f.txt
-- runhaskell project1.hs < f.txt

-- HASKELL PROJECT
-- https://www.ic.unicamp.br/~wainer/cursos/2s2019/346/proj-haskell.html

------------------------------------------------------------------------
import qualified Data.Map.Strict as Map
import Data.List as List
------------------------------------------------------------------------

main = do 
    input <- getContents
    let output = proc input
    putStrLn output

-- proc returns a string with the mapping from label to list of (ordered) points with that label
proc :: String -> String
proc input = let
    (pLines,lLines) = split (lines input) ""
    ps = buildPoints pLines -- list of points with their coordinates
    ds = listAllDistances ps -- list of distances between all points
    m = buildMap lLines -- map from point name to label
    mProcessed = groupLabels ds m
    in showMap $ nameToLabelMap mProcessed

-- groupLabels processes the distances so that we group points in labels by shorter distance
groupLabels :: [(Float,String,String)] -> Map.Map String String -> Map.Map String String
groupLabels [] m = m -- while list of distances is not empty
groupLabels ds m = let
    nv = nextValidPoint ds m -- get next valid distance
    mUpdated = updateLabelMap nv m -- update map with the new labeled point
    dsClear = clearList ds m -- clearList from old/useless points
    in groupLabels dsClear mUpdated

-- takes input lines with names and their coordinates and builds points
buildPoints :: [String] -> [Point]
buildPoints lines = buildPoints' lines []
buildPoints' [] acc = acc
buildPoints' (line:ls) acc = buildPoints' ls (acc++[p])
    where 
    (name:coordinates) = words line
    p = Point name (foldl (\cds x -> cds++[(read x :: Float)]) [] coordinates)

-- takes input lines with labeled names and builds map
buildMap :: [String] -> Map.Map String String
buildMap lines = buildMap' lines (Map.fromList [])
buildMap' [] acc = acc
buildMap' (line:ls) acc = buildMap' ls (Map.insert name label acc)
    where 
    (name:rest) = words line
    label = head rest

-- split lista antes e depois do elemento e
split :: (Eq e) => [e] -> e -> ([e],[e])
split l e = split' l e []
split' [] _ acc = (acc,[])
split' (x:xs) e acc
    | x==e = (acc,xs)
    | otherwise = split' xs e (acc++[x])

-- updateLabelMap inserts a new labeled point in the map
updateLabelMap :: (Float,String,String) -> Map.Map String String -> Map.Map String String
updateLabelMap (d,n1,n2) m = Map.insert n2 label m where label = m Map.! n1

-- nextValidPoint finds the next distance between a labeled point (first) and a non labeled point (second)
nextValidPoint :: [(Float,String,String)] -> Map.Map String String -> (Float,String,String)
nextValidPoint ((d,n1,n2):xs) m
    | Map.member n1 m = (d,n1,n2)
    | otherwise = nextValidPoint xs m

-- showMap converts from Map to String
showMap :: (Show k, Show v) => Map.Map k v -> String
showMap = List.intercalate "\n" . map show . Map.toList

-- builds a map (label -> list of point names) from a map (name -> label)
nameToLabelMap :: Map.Map String String -> Map.Map String [String]
nameToLabelMap m = let
    mapNonSorted = foldl (\mOut (n,l) -> Map.insertWith (++) l [n] mOut) (Map.fromList []) (Map.toList m)
    mapSorted = foldl (\mOut (l,ns) -> Map.insert l (sort ns) mOut) (Map.fromList []) (Map.toList mapNonSorted)
    in mapSorted

-- clear the list of distances, removing points both in the labeled set or distances between a point and itself
clearList :: [(Float,String,String)] -> Map.Map String String -> [(Float,String,String)]
clearList l m = filter (\ (d,n1,n2) -> not (n1==n2 || ((Map.member n1 m) && (Map.member n2 m)))) l

listAllDistances :: [Point] -> [(Float,String,String)]
-- listAllDistances lists distances between all points to all points
listAllDistances p = sort (listAllDistances' p p [])
listAllDistances' _ [] acc = acc
listAllDistances' p (x:xs) acc = listAllDistances' p xs (acc++(listDistances x p))
-- listDistances lists distances between a point and all the points in a list
listDistances x p = foldl (\acc y -> acc++[distance x y]) [] p

-- Point contains a name (String) and a list of coordinates ([Float])
data Point = Point String [Float] deriving (Eq,Read,Show)
-- tuple: distance between two points and the names of the points
distance :: Point -> Point -> (Float, String, String)
distance (Point n1 x1) (Point n2 x2) = (dist x1 x2, n1, n2)
-- distance between two lists
dist :: (Floating a) => [a] -> [a] -> a
dist x y = sqrt $ sum $ map (**2) $ zipWith (-) x y
