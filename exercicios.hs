module Exercicios where

-- author: Carolina de Senne Garcia
-- desennecarol@gmail.com

-- Para complilar este arquivo:
-- $ ghc -o exercicios exercicios.hs

-- Para rodar este arquivo:
-- $ ./exercicios

-- Para carregar no ghci:
-- ghci exercicios.hs

-- AULAS 01 e 02

-- tamanho de uma lista
size [] = 0
size (x:xs) = 1 + size xs

-- soma dos elementos de uma lista
suml [] = 0
suml (x:xs) = x + (suml xs)

-- soma com acumulador
sum' l = sumacc l 0
sumacc [] acc = acc
sumacc (x:xs) acc = sumacc xs (x+acc)

-- soma dos pares de uma lista
sumeven l = sumeven' l 0
sumeven' [] acc = acc
sumeven' (x:xs) acc
    | mod x 2 == 0 = sumeven' xs (x+acc)
    | otherwise = sumeven' xs acc

-- ultimo elemento da lista
lastelement (x:xs) 
    | xs==[] = x
    | otherwise = lastelement(xs)

-- lista contem elemento e
containse [] _ = False
containse (x:xs) e
    | x==e = True
    | otherwise = containse xs e

-- lista de n a 1
geralista n
    | n==0 = []
    | otherwise = n : (geralista (n-1))

-- posicao elemento e na lista l
posicao [] _ = 0
posicao (x:xs) e
    | e==x = 1
    | otherwise = if posrest==0 then 0 else posrest+1
    where posrest = posicao xs e

-- posicoes elemento e na lista l
posicoes:: (Eq a, Num b) => a -> [a] -> [b]
posicoes e l = posicoes' e l [] 0
posicoes' _ [] acc _ = acc
posicoes' e (x:xs) acc i
    | x==e = posicoes' e xs (acc++[i]) (i+1)
    | otherwise = posicoes' e xs acc (i+1)

-- soma elementos em posicoes pares
somapospares l = somapospares' l 1 0
somapospares' [] _ acc = acc
somapospares' (x:xs) i acc
    | mod i 2 == 0 = somapospares' xs (i+1) (acc+x)
    | otherwise = somapospares' xs (i+1) acc

-- soma pares com list comprehension
somapareslc xs = sum' [x | x <- xs, x `mod` 2 == 0]

-- interlacar duas listas
intercala l1 [] = l1
intercala [] l2 = l2
intercala (x1:xs1) (x2:xs2) = x1 : x2 : (intercala xs1 xs2)

-- lista esta ordenada
isSorted [] = True
isSorted[x] = True
isSorted (x:y:xs) = x <= y && isSorted (y:xs)

-- shift right
shiftr l = shiftr' l []
shiftr' [] acc = acc
shiftr' (x:xs) acc
    | xs==[] = x:acc
    | otherwise = (shiftr' xs (acc++[x]))

-- shift left
shiftl [] = []
shiftl (x:xs) = xs ++ [x]

-- remove elemento da lista
remove [] _ = []
remove (x:xs) e
    | x==e = remove xs e
    | otherwise = x:(remove xs e)

-- split lista antes e depois do elemento e
split l e = split' l e []
split' [] _ acc = [acc,[]]
split' (x:xs) e acc
    | x==e = [acc,xs]
    | otherwise = split' xs e (acc++[x])

-- conta numero de itens numa lista
contait it lista = contait' it lista 0
contait' it [] acc = acc
contait' it (x:xs) acc
    | x==it = contait' it xs (acc+1)
    | otherwise = contait' it xs acc


-- AULA 3

-- troca a primeira ocorrencia de v por n
troca1 :: Eq a => a -> a -> [a] -> [a]
troca1 _ _ [] = []
troca1 v n (x:xs)
    | x==v = n:xs
    | otherwise = x:(troca1 v n xs)


-- AULA 4

data Point = Point Float Float deriving (Eq,Read,Show)
-- distance between two points
distance (Point x1 y1) (Point x2 y2) = sqrt((x2-x1)**2 + (y2-y1)**2)
-- call example: distance (Point 1 0) (Point 2 0)

data Tree a = Empty | Node a (Tree a) (Tree a) deriving (Eq,Read,Show)

-- retorna true se a arvore eh uma BST (binary search tree)
isBST :: Ord a => Tree a -> Bool
isBST Empty = True
isBST (Node _ Empty Empty) = True
isBST (Node x Empty ar) = isBST ar && x < smallest ar
isBST (Node x al Empty) = isBST al && x > biggest al
isBST (Node x al ar) = isBST al && isBST ar && x < smallest ar && x > biggest al
-- call example: isBST (Node 3 (Node 1 (Empty) (Empty)) (Node 2 (Empty) (Empty)))

-- menor elemento de uma BST
smallest (Node x Empty _) = x
smallest (Node x al _) = smallest al

-- maior elemento de uma BST
biggest (Node x _ Empty) = x
biggest (Node x _ ar) = biggest ar

-- verifica se um elemento e existe em uma BST
findBST :: Ord a => a -> Tree a -> Bool
findBST _ Empty = False
findBST e (Node x al ar) = x==e || (findBST e al) || (findBST e ar)

-- calcula a profundidade maxima de uma BST
depth :: Ord a => Tree a -> Int
depth Empty = 0
depth (Node x al ar) = 1 + (max (depth al) (depth ar))

-- coverte uma BST numa lista em ordem infixa (arvore-esquerda, no, arvore-direita)
convertBSTtoInfix :: Ord a => Tree a -> [a]
convertBSTtoInfix Empty = []
convertBSTtoInfix (Node x al ar) = (convertBSTtoInfix al)++[x]++(convertBSTtoInfix ar)

-- insere um item numa BST
insertBST :: Ord a => a -> Tree a -> Tree a
insertBST e Empty = (Node e (Empty) (Empty))
insertBST e (Node x al ar)
    | e==x = (Node e al ar)
    | e < x = (Node x (insertBST e al) ar)
    | e > x = (Node x al (insertBST e ar))

-- AULA 5
-- usar foldl, filter, map, flip e pipe ($) da direita para a esquerda

-- tamanho de uma lista
sizer l = foldr (\ _ n -> 1+n) 0 l -- foldr : elemento da lista vem primeiro
sizel l = foldl (\ n _ -> 1+n) 0 l -- foldl : elemento da lista vem depois do acumulador

-- soma dos pares de uma lista
somapares l = foldl (+) 0 $ filter (\ x -> x `mod` 2 == 0) l

-- conta numero de ocorrencias de e em l
conta e l = foldr (\ x count -> if x==e then (count+1) else count) 0 l

-- existe item e na lista (True ou False)
exists :: Eq a => a -> [a] -> Bool
exists e l = foldl (\ b x -> b || (x==e)) False l

exists' :: Eq a => a -> [a] -> Bool
exists' e l = notEmpty $ filter (\ x -> x==e) l

notEmpty :: [a] -> Bool
notEmpty [] = False
notEmpty (x:xs) = True

-- maior elemento de uma lista
maiorE :: Ord a => [a] -> a
maiorE l = foldl1 (\ m x -> if x > m then x else m) l

-- take n lista - os primeiros n elementos da lista
nFirst n l = foldl (\ newL x -> if (size newL)==n then newL else (newL++[x])) [] l

-- splitall sublistas de uma lista split com elemento e dado -- COMO FAZER COM FUNCOES DA AULA 5?
splitall :: (Eq a) => a -> [a] -> [[a]]
splitall e l = splitall' e l [] [] 
splitall' _ [] acc res = res++[acc]-- accumulador eh a lista atual, resultado é o resultado da lista de listas
splitall' e (x:xs) acc res
    | x==e = splitall' e xs [] (res++[acc])
    | otherwise = splitall' e xs (acc++[x]) res

-- transposta de uma matriz
transp :: [[a]] -> [[a]]
transp ([]:_) = []
transp m = (map head m) : (transp (map tail m))

-- multiplica duas matrizes
matmul :: (Num a) => [[a]] -> [[a]] -> [[a]]
matmul m1 m2 = matmul' m1 (transp m2)
matmul' [] _ = []
matmul' (line:rest) m2 = map (\l -> dotprod l line) m2 : (matmul' rest m2)

dotprod l1 l2 = sum' $ zipWith (*) l1 l2


