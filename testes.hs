module Testes where

-- questoes de testes

-- author: Carolina de Senne Garcia
-- desennecarol@gmail.com

-- Para complilar este arquivo:
-- $ ghc -o testes testes.hs

-- Para rodar este arquivo:
-- $ ./testes

-- Para carregar no ghci:
-- ghci testes.hs

import Data.Char (toLower)
import qualified Data.Set as Set
import qualified Data.List as List
import qualified Data.Map.Strict as Map

-- teste AULA 7
data Tree ch v = Vazia | No ch v (Tree ch v) (Tree ch v) deriving (Eq,Read,Show)

insereAbb :: (Ord ch) => ch -> v -> Tree ch v -> Tree ch v
insereAbb key val Vazia = (No key val (Vazia) (Vazia))
insereAbb key val (No ch v tl tr)
    | key==ch = (No key val tl tr)
    | key < ch = (No ch v (insereAbb key val tl) tr)
    | key > ch = (No ch v tl (insereAbb key val tr))

-- teste AULA 8
soma1 :: (Eq a) => a -> [(a,Int)] -> [(a,Int)]
soma1 ch [] = [(ch,1)]
soma1 ch ((a,b):xs) 
    | ch == a = (a,b+1):xs
    | otherwise = (a,b):soma1 ch xs

-- retorna vogal mais comum
vogalmaiscomum s = vogalmaiscomum' s []
vogalmaiscomum' [] acc = getMax acc
vogalmaiscomum' (x:xs) acc
    | isVogal (toLower x) = vogalmaiscomum' xs (soma1 (toLower x) acc)
    | otherwise = vogalmaiscomum' xs acc

getMax tps = getMax' tps ' ' 0
getMax' [] acc max = acc
getMax' ((e,f):xs) acc max
    | f > max = getMax' xs e f
    | otherwise = getMax' xs acc max

isVogal c = c=='a' || c=='e' || c=='i' || c=='o' || c=='u'
soVogais s = filter (isVogal . toLower) s

vogalmais l = let
    v1 = filter (`elem` "aeiou") $ map toLower l
    v2 = foldl (flip soma1) [] v1
    v3 = snd $ maximum [(b,a) | (a,b) <- v2]
    in v3

vogalmais' l = snd $ maximum [(b,a) | (a,b) <- (foldl (flip soma1) [] (filter (`elem` "aeiou") $ map toLower l))]

-- TESTE AULA 10

main = do 
    input <- getContents
    let output = proc' input
    putStrLn output

-- versao melhorada
proc' input = let
    wds = words (map toLower $ (foldl(\ acc c -> if elem c ",.:;!?\n" then acc++[' '] else acc++[c]) [] input))
    set = foldl (\ acc w -> Set.insert w acc) (Set.fromList []) wds
    in show (Set.size set)

proc input = let
    allWords = computeAllWords input
    myMap = buildMap allWords
    in show (Map.size myMap)

buildMap :: [String] -> Map.Map String String
buildMap lines = buildMap' lines (Map.fromList [])
buildMap' [] acc = acc
buildMap' (word:ls) acc = buildMap' ls (Map.insert (lowerWord word) "" acc)

lowerWord w = foldl (\ acc c -> acc++[(toLower c)]) [] w

computeAllWords :: String -> [String]
computeAllWords input = foldl (\ aw line -> aw++(filter (\w -> w /= "") (splitOneOf " ,.:;!?\n" line)) ) [] (lines input)

-- splitall sublistas de uma lista split com elemento e dado -- COMO FAZER COM FUNCOES DA AULA 5?
splitOneOf e l = splitall' e l [] [] 
splitall' _ [] acc res = res++[acc]-- accumulador eh a lista atual, resultado é o resultado da lista de listas
splitall' e (x:xs) acc res
    | exists x e = splitall' e xs [] (res++[acc])
    | otherwise = splitall' e xs (acc++[x]) res

exists e l = foldl (\ b x -> b || (x==e)) False l




